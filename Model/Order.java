package Model;


import java.util.HashMap;
import java.util.Map; 

public class Order {
	private int idOrder;
    private Client client;
    private Map<Product, Integer> prod;
    
    public Order(int id, Client idC) {
    	this.idOrder=id;
    	this.client=idC;
    	prod=new HashMap<Product, Integer>(); 
    }
    
	
    public void setId(int id) {
    	this.idOrder=id;
    }
    
 
    public int getId() {
    	return this.idOrder;
    }
    
    public void setProduse(Product p, int cantitate) {
    	prod.put(p, cantitate);	
    }
    
    
    public int getNrProduse() {
    	return prod.size();
    }
    
    public void setClient(Client c) {
    	this.client=c;
    }
    public Client getClient() {
    	return this.client;
    }
    
    public String getProdus() {
    	String pr=new String();
    	pr="";
    	for(Product p: prod.keySet()) {
    		pr+=p.getName();	
    	}
     return pr;
    }
   
    
     public int getCantitate(Product p) {
    	return prod.get(p);
    }
  
    
    
    public String getProd() {
    	String pr=new String();
    	pr="";
    	for(Product p: prod.keySet()) {
    		pr+=p.getName()+ "-cantitate:" + prod.get(p)+ "\n";
    		
    	}
    	return pr;
    }
    
   
    
   
	
}
