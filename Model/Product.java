package Model;

public class Product {
    private int id;
    private String name;
    private int quantity;
    
    
    public Product(int id,String name,int quantity) {
    	this.id=id;
    	this.name=name;
    	this.quantity=quantity;
    }
    
    public void setId(int id) {
    	this.id=id;
    }
    
    public void setName(String name) {
    	this.name=name;
    }
    
    
    public void setQuantity(int quantity) {
    	this.quantity=quantity;
    }
    
    public int getId() {
    	return this.id;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public int getQuantity() {
    	return this.quantity;
    }
    
    public String toString() {
    	String s=new String();
		s="";
		s+= id +","+name+ ","+quantity;
		return s;
    }
    
}
