package Model;

public class Client {
    private int id;
    private String name;
    private String address;
    private String email;
	
    
    public Client(int id,String name,String address,String email) {
    	this.id=id;
    	this.name=name;
    	this.address=address;
    	this.email=email;
    }
    
    
    public void setId(int id)
    {
    	this.id=id;
    }
    
    public int getId() 
    {
    	return this.id;
    }
    
    public void setName(String name)
    {
    	this.name=name;
    }
    public String getName()
    {
    	return this.name;
    }
    
    public void setAddress(String address)
    {
    	this.address=address;
    }
    public String getAddress()
    {
    	return this.address;
    }
    
	public void setEmail(String email) 
	{
		this.email=email;
	}
	public String getEmail()
	{
		return this.email;
	}
	
	public String toString() {
		String s=new String();
		s="";
		s+= id +","+name+ ","+address+ ","+email;
		return s;
	} 
	
	
	
}
