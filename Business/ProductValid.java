package Business;


import Model.Product;

public class ProductValid {
      private int nameValid(String name) {
    	  int ok=1;
    	  if(name.equals(""))
    		  ok=0;
    	  return ok;
      }
      
      private int quantityValid(int quantity) {
    	  int ok=0;
    	  if(quantity>0)
    	     ok=1;
    	  return ok;
      }
      
      
      
      
      public Product getProduct(String name, int quantity) {
    	  Product p=null;
    	  if(nameValid(name)==1 && quantityValid(quantity)==1)
    		  p=new Product(0,name,quantity);
    	  
    	  return p;
    	  
      }
}