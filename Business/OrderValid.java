package Business;


import Model.Product;
import Model.Client;
import Model.Order;

public class OrderValid {

	private int quantityValid(Product produs,int q) {
		int ok=0;
		if(q<=produs.getQuantity()) {
			ok=1;
		}
		return ok;
	}
	
	public Order getOrder(Client idC,Product produs,int q) {
		Order o=null;
		if(quantityValid(produs,q)==1) {
		 	o=new Order(0,idC);
		 	o.setProduse(produs,q);
		}
		return o;
	}
	
	
}
