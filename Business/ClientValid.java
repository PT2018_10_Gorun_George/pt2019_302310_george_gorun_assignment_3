package Business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.Client;

public class ClientValid {
   
	private int nameValid(String name) {
    	if(name.equals("")) {
    		return 0;
    	}
    	else return 1;
    }
	
	
	private int addressValid(String address) {
		if(address.equals("")) {
			return 0;
		}
		else return 1;
	}
	
	private int emailValid(String email) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches())
           return 1;
        else return 0;
		
	}
	
	public Client getClient(String name, String address,String email) {
		Client c=null;
		if(nameValid(name)==1 && addressValid(address)==1&&emailValid(email)==1)
			c=new Client(0,name,address,email);
		
		return c;
	}
	
}
