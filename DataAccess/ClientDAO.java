package DataAccess;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import Model.Client;

public class ClientDAO {
	private Connection conect;

    public ClientDAO(Connection conect) {
		this.conect = conect;
	}

	public int Insert(Client c){
		int ok=0;
		String add="INSERT INTO client VALUES("+c.getId()+",'"+c.getName()+"','"+c.getAddress()+"','"+c.getEmail()+"')";
		try{
			Statement s=conect.createStatement();
			s.executeUpdate(add);
			ok=1;
		}
		catch (SQLException e)
		{
			System.out.println("Error!");
		}
		return  ok;
	}


	public int Update(Client c) {
		int ok=0;
		String up="UPDATE client SET Name = '" + c.getName() + "', Address= '" + c.getAddress() + "', Email = '" + c.getEmail() + "' where idClient = " + c.getId() +";" ;
		try {
			Statement s=conect.createStatement();
			s.executeUpdate(up);
			ok=1;
		}
		catch (SQLException e)
		{
			System.out.println("Error!");
		}

		return ok;
	}

	public int Delete(Client c) {
		int ok=0;
		String del="DELETE FROM client WHERE idClient="+c.getId();
		try {
			Statement s=conect.createStatement();
			s.executeUpdate(del);
			ok=1;
		}
		catch(SQLException e) {

			System.out.println("Error!");
		}
		return ok;
	}

	public Client findClient(int id) {
		Client c=null;
		String sel="SELECT * FROM client WHERE idClient="+id+";";
		try {
			Statement s = conect.createStatement();
			ResultSet r=s.executeQuery(sel);
			while(r.next())
			{
				c = new Client(r.getInt("idClient"),r.getString("Name"), r.getString("Address"), r.getString("Email"));
			}
		} catch (SQLException e) {
			System.out.println("Error!");
		}
		return  c;
	}


	public ArrayList<Client> ViewAll(){
		ArrayList<Client> results = new ArrayList<Client>();
		String sel="SELECT * FROM client ORDER BY idClient;";
		try {
			Statement state = conect.createStatement();
			ResultSet s = state.executeQuery(sel);
			while(s.next())
			{
				Client c = new Client(s.getInt("idClient"),s.getString("Name"), s.getString("Address"), s.getString("Email"));
				results.add(c);
			}
		} catch (SQLException e) {
			System.out.println("Error!");
		}
		return results;
	}
}
