package DataAccess;



import java.sql.*;
import java.util.logging.Logger;


public class Conectare {
    
    
    public static final Logger LOGGER=Logger.getLogger(Conectare.class.getName());
    public static final String DBURL = "jdbc:mysql://127.0.0.1:3306/myConn";
    public static final String USER = "root";
    public static final String PASSWORD = "root";
   // public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String DRIVER="com.mysql.cj.jdbc.Driver";
    private static Conectare instance = new Conectare();
    
    private Conectare()
    {
        try
        {
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private Connection createConnection()
    {
        Connection connection = null;
        try
        {
            connection = DriverManager.getConnection(DBURL, USER, PASSWORD);
        }
        catch (SQLException e)
        {
            System.out.println("Can't connect to the database!");
        }
        return connection;
    }

    public static Connection getConnection()
    {
        return instance.createConnection();
    }
    
    public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("Can't connect to the database!");
			}
		}
	}

	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				System.out.println("Can't connect to the database!");
			}
		}
	}

	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				System.out.println("Can't connect to the database!");
			}
		}
	}

}

