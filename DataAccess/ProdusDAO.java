package DataAccess;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import Model.Product;

public class ProdusDAO {
		 private Connection conect;

		    public ProdusDAO(Connection conect) {
		        this.conect = conect;
		    }

		    public int Insert(Product p){
		    	int ok=0;
		    	String add="INSERT INTO product VALUES("+p.getId()+",'"+p.getName()+"','"+p.getQuantity()+"')";
		        try{
		        	Statement s=conect.createStatement();
		            //s.executeUpdate("insert into product values("+p.getId()+",'"+p.getName()+"','"+p.getQuantity()+"')");
		            s.executeUpdate(add);
		        	ok=1;
		        }
		        catch (SQLException e)
		        {
		            System.out.println("Error!");
		        }
		        return ok;
		    }
		    
		    
		    public int Update(Product p) {
		    	int ok=0;
		    	String up="update product set Name = '" + p.getName() + "', Quantity= '" + p.getQuantity() + "' where idProduct = " + p.getId();
		    	try {
		    		Statement s=conect.createStatement();
		    		//s.executeUpdate("update product set Name = '" + p.getName() + "', Quantity= '" + p.getQuantity() + "' where idProduct = " + p.getId());
		    		s.executeUpdate(up);
		    		ok=1;
		    	}
		    	catch (SQLException e)
		    	{
		    		System.out.println("Error!");
		    	}
		    	return ok;
		   
		    }
		    
		    public int Delete(Product p) {
		    	int ok=0;
		    	String del="DELETE FROM product WHERE idProduct="+p.getId()+";";
		    	try {
		    		Statement s=conect.createStatement();
		    		//s.executeUpdate("delete from product where idProduct="+p.getId()+";");
		    		s.executeUpdate(del);
		    		ok=1;
		    	}
		    	catch(SQLException e) {
		    		
		    		System.out.println("Error!");
		    	}
		    	return ok;
		    }
		    
		    
		    public Product findProdus(int id) {
		        Product p=null;
		        String sel="SELECT * FROM product where idProduct="+id+";";
		    	try {
		            Statement s = conect.createStatement();
		            //ResultSet s = state.executeQuery("SELECT * FROM product where idProduct="+id+";");
		            ResultSet r=s.executeQuery(sel);
		            while(r.next())
		            {
		              p = new Product(r.getInt("idProduct"),r.getString("Name"), r.getInt("Quantity"));
		            }
		        } catch (SQLException e) {
		            System.out.println("Error!");
		        }
		        return  p;
		    	
		    	
		    }
		    
		    public ArrayList<Product> ViewAll(){
		        ArrayList<Product> res = new ArrayList<Product>();
		        try {
		            Statement s = conect.createStatement();
		            ResultSet r = s.executeQuery("SELECT * FROM product order by idProduct;");
		            while(r.next())
		            {
		                Product p = new Product(r.getInt("idProduct"),r.getString("Name"), r.getInt("Quantity"));
		                res.add(p); 
		            }
		        } catch (SQLException e) {
		            System.out.println("Data Base Error!!!");
		        }
		        return res;
		    }
	}

	
	