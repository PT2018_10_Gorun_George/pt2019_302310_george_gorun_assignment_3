package Start;


import Presentation.GuiClient;
import Presentation.GuiOrder;
import Presentation.GuiProdus;

public class Start {
	public static void main(String args[]) {
    	GuiClient client=new GuiClient();
    	GuiOrder order=new GuiOrder();
    	GuiProdus produs=new GuiProdus();
    
    	
    	client.clientPanel();
    	produs.produsPanel();
    	order.orderPanel();
    } 
}
