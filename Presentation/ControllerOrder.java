package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import Business.OrderValid;
import DataAccess.ClientDAO;
import DataAccess.Conectare;
import DataAccess.OrderDAO;
import DataAccess.ProdusDAO;
import Model.Client;
import Model.Order;
import Model.Product;



public class  ControllerOrder implements ActionListener{
       public GuiOrder g;
       public Connection con;
       public ClientDAO clientDAO;
       public ProdusDAO produsDAO;
       public OrderDAO order;
       public OrderValid ordervalid;
       
       
	public ControllerOrder(GuiOrder g) {
			this.g=g;
			con=Conectare.getConnection();
			order=new OrderDAO(con);
			ordervalid=new OrderValid();
			clientDAO=new ClientDAO(con);
			produsDAO=new ProdusDAO(con);

	}
	
	public void actionPerformed(ActionEvent e) {
		
		Object obiect = (Object)e.getSource();
		JButton button = (JButton)obiect; 
	
		
		Product pr=g.getProduct();
		Client cl=g.getClient();
		String q=g.getQuantity();
		
		if(button.getActionCommand()=="Adauga Comanda") {
			int q1=Integer.parseInt(q);
			Order o=ordervalid.getOrder(cl, pr, q1);
		    if(o!=null) {
		    	if(order.Create(cl,o,pr)==1) { 
		    		pr.setQuantity(pr.getQuantity()-q1);
		    		JOptionPane.showMessageDialog(null," Comanda adaugata!");
		    	}
		    	else
		    		JOptionPane.showMessageDialog(null," Comanda nu s-a executat!");
		    }
		  	else {
		    
		   		JOptionPane.showMessageDialog(null,"Produse insuficiente sau stoc epuizat!");
		   	}   		   
		}
		}
		
}
		
	