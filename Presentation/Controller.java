package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.util.*;
import Business.ClientValid;
import DataAccess.ClientDAO;
import DataAccess.Conectare;
import Model.Client;



public class  Controller implements ActionListener{
       public GuiClient g;
       public Connection con;
       public ClientDAO clienti;
       public ClientValid clientvalid;
       
       
	public Controller(GuiClient g) {
			this.g=g;
			con=Conectare.getConnection();
			clienti=new ClientDAO(con);
			clientvalid=new ClientValid();

	}
	
	public void actionPerformed(ActionEvent e) {
		
		Object obiect = (Object)e.getSource();
		JButton button = (JButton)obiect; 
	
		
		String name=g.getName();
		String address=g.getAddress();
		String Email=g.getEmail();
		Client c=clientvalid.getClient(name, address, Email);
		
		switch(button.getActionCommand()) {
		case "Adauga":
			if(c!=null) {
				if(clienti.Insert(c)==1) {
					JOptionPane.showMessageDialog(null," Client adaugat cu succes !");
				}
				else JOptionPane.showMessageDialog(null," Verificati Datele de intrare!");
			}
			g.reset();
			break;
		case "Sterge":
			
			int idC=g.getId();
			Client p2=clienti.findClient(idC);
			if(p2!=null) {
				clienti.Delete(p2);
			}
			 else{
				 JOptionPane.showMessageDialog(null,"Index inexistent!");
             }
			
			g.reset();
			break;	
		case "Modifica":
			
			int idC1=g.getId();
			
			if(clienti.findClient(idC1)!=null && c!=null) {
				c.setId(idC1);
				clienti.Update(c);
			}
			else {
				JOptionPane.showMessageDialog(null,"Index inexistent!");

			}
			
			g.reset();
			break;		
		case "Afisaza Clienti":
			 ArrayList<Client> l=new ArrayList<Client>();
			 l=clienti.ViewAll();
			 g.update(l);
            break;
		default: 
			break;
    }
		
}
	
}	
	