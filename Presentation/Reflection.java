package Presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class Reflection {
    
	public static String[] getColumns(Object obj) {	
		Class cls=obj.getClass();
		Field[] columns=cls.getDeclaredFields();
		String[] nameC=new String[columns.length];
		int pos=0;
		
		for(Field f:columns) {
			nameC[pos++]=f.getName();
		}
		return nameC;
		
	}
	
	public static Object[][] getValues(ArrayList<?> arr){
    	 Class cls=arr.getClass();
    	 Field[] columns=cls.getDeclaredFields();
    	 int c=columns.length;
    	 int r=arr.size();
    	 Object val[][]=new Object[r][c];
    	 int i=0;
    	 for(Object obj:arr) {
    		 int j=0;
    		 for(Field f:obj.getClass().getDeclaredFields()) {
    			  f.setAccessible(true);
    			  Object value;
    			  try {
    				  
    				  val[i][j]=f.get(obj);
    			  }catch(IllegalArgumentException e) {
    				  System.out.println("1");
    			  }catch(IllegalAccessException e) {
    				  System.out.println("2");  
    			  }
    			  j++;
    		 }
    		 i++;		 
    	 }
    	 return val;
     }
	
	
	
}
