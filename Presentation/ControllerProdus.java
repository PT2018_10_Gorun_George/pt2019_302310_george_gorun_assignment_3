package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;


import javax.swing.JButton;
import javax.swing.JOptionPane;


import Business.ProductValid;
import DataAccess.ProdusDAO;
import DataAccess.Conectare;
import Model.Product;

public class  ControllerProdus implements ActionListener{
    public GuiProdus g;
    public Connection con;
    public ProdusDAO produs;
    public ProductValid produsvalid;
    
    
	public ControllerProdus(GuiProdus g) {
			this.g=g;
			con=Conectare.getConnection();
			produs=new ProdusDAO(con);
			produsvalid=new ProductValid();

	}
	
	public void actionPerformed(ActionEvent e) {
		
		Object obiect = (Object)e.getSource();
		JButton buton = (JButton)obiect; 
		
		String prod=g.getName();
		String quant=g.getQuantity();
		
		
		
		switch(buton.getActionCommand()) {
		case "Adauga":
			int quan=Integer.parseInt(quant);
			Product p=produsvalid.getProduct(prod, quan);
			if(p!=null) {
				if(produs.Insert(p)==1) {
					JOptionPane.showMessageDialog(null," Produs adaugat cu succes !");
				}
				else JOptionPane.showMessageDialog(null," Verificati Datele de intrare!");
			}
			g.reset();
		    break;
		case "Sterge":
			
			int idP=Integer.parseInt(g.getId());
			Product p2=produs.findProdus(idP);
			if(p2!=null) {
				produs.Delete(p2);
			}
			 else{
				 JOptionPane.showMessageDialog(null,"Index inexistent!");
             }
			
			g.reset();
			break;	
		case "Modifica":
			int idP1=Integer.parseInt(g.getId());
			int quan1=Integer.parseInt(quant);
			Product p1=produsvalid.getProduct(prod, quan1);
			
			if(produs.findProdus(idP1)!=null && p1!=null) {
				p1.setId(idP1);
				produs.Update(p1);
			}
			
			g.reset();
			break;		
		case "Afisaza Produse":
			 ArrayList<Product> l=new ArrayList<Product>();
			 l=produs.ViewAll();
			 g.update(l);
            break;
		default: 
			break;
    }
		
}
	
}	
	
