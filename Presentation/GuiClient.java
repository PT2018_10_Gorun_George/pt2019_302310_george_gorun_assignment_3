package Presentation;


import java.awt.Color;
import java.util.ArrayList;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import Model.Client;



public class GuiClient extends JFrame {
  
	private static final long serialVersionUID = 1L;
	private JTable table1;
	private JScrollPane scroll1;
    private JPanel info;
    private JPanel tabel;
    private JLabel id;
    private JLabel name;
    private JLabel address;
    private JLabel Email;
    private JButton AdaugaClient;
    private JButton StergeClient;
    private JButton ModificaClient;
    private JButton AfisazaClienti;
    private DefaultTableModel Model;
    private JTextArea idT;
    private JTextArea numeT;
    private JTextArea addr;
    private JTextArea em;
    
    
    
    public GuiClient() {
		
	}

    
 
    
    public void clientPanel() {
    	setTitle("Clienti");
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBackground(Color.gray);
    	setLayout(null);
        
    	
    	info=new JPanel();
    	
    	info.setBounds(0,0,200,600);
    	info.setBackground(Color.gray);
    	info.setLayout(null);
    	
    	id=new JLabel("ID");
    	id.setBounds(20,20,80,20);
    	
    	
    	idT=new JTextArea();
    	idT.setBounds(70,20,100,20);
    	info.add(id);
    	info.add(idT);
    	
    	name=new JLabel("Nume");
    	name.setBounds(20,60,80,20);
    	info.add(name);
    	
    	numeT=new JTextArea();
    	numeT.setBounds(70,60,100,20);
    	info.add(numeT);
    	
    	address=new JLabel("Adresa");
    	address.setBounds(20,100,80,20);
    	info.add(address);
    	
    	addr=new JTextArea();
    	addr.setBounds(70,100,100,20);
    	info.add(addr);
    	
    	Email=new JLabel("Email");
    	Email.setBounds(20,140,80,20);
        info.add(Email);
        
    	em=new JTextArea();
    	em.setBounds(70,140,100,20);
    	info.add(em);
    	
    	
    	Controller c=new Controller(this);
    	AdaugaClient= new JButton("Adauga");
    	AdaugaClient.setBounds(30,200,150,40);
    	AdaugaClient.addActionListener(c);
    	
    	
    	info.add(AdaugaClient);
    	
    	ModificaClient= new JButton("Modifica");
    	ModificaClient.setBounds(30,250,150,40);
    	ModificaClient.addActionListener(c);
    	info.add(ModificaClient);
    	
    	StergeClient= new JButton("Sterge");
    	StergeClient.setBounds(30,300,150,40);
    	StergeClient.addActionListener(c);
    	info.add(StergeClient);
    	
    	AfisazaClienti=new JButton("Afisaza Clienti");
    	AfisazaClienti.setBounds(30,350,150,40);
    	AfisazaClienti.addActionListener(c);
    	info.add(AfisazaClienti);
    	
    	
    	
        tabel=new JPanel();
    	
    	tabel.setBounds(200,0,600,600);
    	tabel.setBackground(Color.lightGray);
    	
    
    	add(info);
    	add(tabel);
    	
    	
    	
    	
    	setVisible(true);
    }
    
    
    
    public String getName() {
       return numeT.getText();
    }
    
    public String getAddress() {
    	return addr.getText();
    }
    
    public String getEmail() {
    	return em.getText();
    }
    
    public int getId() {
    	int id=Integer.parseInt(idT.getText());
    	return id;
    }
    
    
    public void reset() {
    	idT.setText("");
    	numeT.setText("");
		addr.setText("");
		em.setText("");
    }
   
    public void createTable() {
    	tabel.removeAll();
    	scroll1=new JScrollPane(table1);
    	tabel.add(scroll1);
    	tabel.repaint();
    	tabel.revalidate();
    	scroll1.setVisible(true);
    	tabel.setVisible(true);
    	
    }
    
	public void update(ArrayList<Client> l) {
		Model=new DefaultTableModel(Reflection.getValues(l),Reflection.getColumns(l.get(0)));
        table1=new JTable(Model);
		createTable();
		
	}


    
	
	
	
	
	
}
