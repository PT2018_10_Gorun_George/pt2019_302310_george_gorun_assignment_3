package Presentation;


import java.awt.Color;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JTextArea;

import DataAccess.ClientDAO;
import DataAccess.Conectare;
import DataAccess.ProdusDAO;
import Model.Client;
import Model.Product;

public class GuiOrder extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel client;
	private JLabel produs;
	private JLabel cantitate;
	private JTextArea cant;
	private JComboBox<Client> cbClient;
	private JComboBox<Product> cbProdus;
	private JButton AdaugaComanda;
	private ClientDAO clientDAO;
	private ProdusDAO produsDAO;
	private Connection con;
	
	public GuiOrder() {
		con=Conectare.getConnection();
		clientDAO=new ClientDAO(con);
		produsDAO= new ProdusDAO(con);
	}
	
	public void orderPanel()
	{
		setTitle("Order");
		setSize(500,500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBackground(Color.DARK_GRAY);
    	setLayout(null);
    	
    	client=new JLabel("Client");
    	client.setBounds(20,20,80,20);
    	
    	cbClient=new JComboBox<Client>();
    	cbClient.setBounds(70,20,250,20);
    	ArrayList<Client> l=new ArrayList<Client>();
    	l=clientDAO.ViewAll();
    	for(Client c: l) {
    		cbClient.addItem(c);
    	}
    	
    	add(client);
    	add(cbClient);
    	
    	produs=new JLabel("Produs");
    	produs.setBounds(20,60,80,20);
    	
    	add(produs);
    	cbProdus=new JComboBox<Product>();
    	cbProdus.setBounds(70,60,250,20);
    	ArrayList<Product> lprod=new ArrayList<Product>();
    	lprod=produsDAO.ViewAll();
    	for(Product p: lprod) {
    		cbProdus.addItem(p);
    	}
    	
    	add(cbProdus);
    	
    	cantitate=new JLabel("Cantitate");
    	cantitate.setBounds(20,100,80,20);
    	add(cantitate);
    	
    	cant=new JTextArea();
    	cant.setBounds(70,100,100,20);
    	add(cant);
    	
    	
    	ControllerOrder c=new ControllerOrder(this);
        AdaugaComanda=new JButton("Adauga Comanda");
    	AdaugaComanda.setBounds(30,200,150,40);
    	AdaugaComanda.addActionListener(c);
    	add(AdaugaComanda);
    	
    	setVisible(true);
	}	
	
    public Product getProduct() {
    	return (Product) cbProdus.getSelectedItem();
    }
	
    public Client getClient() {
    	return (Client) cbClient.getSelectedItem();
    }
    
    public String getQuantity() {
    	return cant.getText();
    	
    }
	
}

