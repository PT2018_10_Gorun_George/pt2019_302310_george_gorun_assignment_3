package Presentation;


import java.awt.Color;
import java.util.ArrayList;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;


import Model.Product;



public class GuiProdus extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable table2;
	private JScrollPane scroll2;
	private JPanel infop;
	private JPanel tabelp;
	private JLabel idp;
	private JLabel Produs;
	private JLabel cantitate;
	private JButton AdaugaProdus;
	private JButton StergeProdus;
	private JButton ModificaProdus;
	
	private JButton AfisazaProduse;
	private DefaultTableModel Model;
	private JTextArea idpr;
	private JTextArea numeP;
	private JTextArea q;
	
	
	public GuiProdus() {
		
		
	}
	
	
	public void produsPanel() {
    	setTitle("Produse");
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBackground(Color.gray);
    	setLayout(null);
        
    	
    	infop=new JPanel();
    	
    	infop.setBounds(0,0,200,600);
    	infop.setBackground(Color.gray);
    	infop.setLayout(null);
    	
    	idp=new JLabel("ID");
    	idp.setBounds(20,20,80,20);
    	
    	
    	idpr=new JTextArea();
    	idpr.setBounds(70,20,100,20);
    	infop.add(idp);
    	infop.add(idpr);
    	
    	Produs=new JLabel("Produs");
    	Produs.setBounds(20,60,80,20);
    	infop.add(Produs);
    	
    	numeP=new JTextArea();
    	numeP.setBounds(70,60,100,20);
    	infop.add(numeP);
    	
    	cantitate=new JLabel("Cantitate");
    	cantitate.setBounds(20,100,80,20);
    	infop.add(cantitate);
    	
    	q=new JTextArea();
    	q.setBounds(70,100,100,20);
    	infop.add(q);
    	
    	ControllerProdus c=new ControllerProdus(this);
    	AdaugaProdus= new JButton("Adauga");
    	AdaugaProdus.setBounds(30,200,150,40);
    	AdaugaProdus.addActionListener(c);
    	
    
    	infop.add(AdaugaProdus);
    	
    	ModificaProdus= new JButton("Modifica");
    	ModificaProdus.setBounds(30,250,150,40);
    	ModificaProdus.addActionListener(c);
    	infop.add(ModificaProdus);
    	
    	StergeProdus= new JButton("Sterge");
    	StergeProdus.setBounds(30,300,150,40);
    	StergeProdus.addActionListener(c);
    	infop.add(StergeProdus);
    	
    	
    	AfisazaProduse=new JButton("Afisaza Produse");
    	AfisazaProduse.setBounds(30,350,150,40);
    	AfisazaProduse.addActionListener(c);
    	infop.add(AfisazaProduse);
    	
    	
    	
        tabelp=new JPanel();
    	
    	tabelp.setBounds(200,0,600,600);
    	tabelp.setBackground(Color.lightGray);
    	
    
    	add(infop);
    	add(tabelp);
    	
    	
    	setVisible(true);
    }


	
	
	
	

    public String getName() {
       return numeP.getText();
    }
    
    
    public String getQuantity() {
      //  int cantitate=Integer.parseInt(q.getText());      	
    	return q.getText();
    }
    
    public String getId() {
    	return idpr.getText();
    }
	
	   public void reset() {
		    idpr.setText("");
			numeP.setText("");
			q.setText("");
	    }
	   
	    public void createT() {
	    	tabelp.removeAll();
	    	scroll2=new JScrollPane(table2);
	    	tabelp.add(scroll2);
	    	tabelp.repaint();
	    	tabelp.revalidate();
	    	scroll2.setVisible(true);
	    	tabelp.setVisible(true);
	    	
	    }
	    
		public void update(ArrayList<Product> l) {
			Model=new DefaultTableModel(Reflection.getValues(l),Reflection.getColumns(l.get(0)));
	        table2=new JTable(Model);
			createT();
			
		}

	
	
}
